#in case of doubts check the notebook link
# https://github.com/udacity/machine-learning/blob/master/projects/practice_projects/naive_bayes_tutorial/Naive_Bayes_tutorial.ipynb
#
import pandas as pd
import string
from collections import Counter
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import train_test_split
import numpy as np
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score

#read the spam file
df = pd.read_table('/home/skineer/udacityExercises/data/SMSSpamCollection',
                   sep='\t', 
                   header=None, 
                   names=['label', 'sms_message'])

#transform categorical to numerical
df['label'] = df.label.map({"ham" : 0, "spam" : 1})
print df.shape

#bag of words from the scratch.
documents = ['Hello, how are you!',
             'Win money, win from home.',
             'Call me now.',
             'Hello, Call hello you tomorrow?']

lower_case_documents = [x.lower() for x in documents]
sans_punctuation_documents = [x.translate(string.maketrans('',''), string.punctuation) for x in lower_case_documents]
preprocessed_documents = [x.split(' ') for x in sans_punctuation_documents]
frequency_list = [Counter(x) for x in preprocessed_documents]

#bag of words using scikit learn
countVector = CountVectorizer()
countVector.fit(documents)
countVector.get_feature_names()
doc_array = countVector.transform(documents).toarray()
frequency_matrix = pd.DataFrame(doc_array, columns = countVector.get_feature_names())
X_train, X_test, y_train, y_test = train_test_split(df['sms_message'], 
                                                    df['label'], 
                                                    random_state=1)
print('Number of rows in the total set: {}'.format(df.shape[0]))
print('Number of rows in the training set: {}'.format(X_train.shape[0]))
print('Number of rows in the test set: {}'.format(X_test.shape[0]))

#start CountVectorizer again to transform the spam dataset
countVector = CountVectorizer()
training_data = countVector.fit_transform(X_train)
testing_data = countVector.transform(X_test)

#the bayes theorem
# Sensitivity or True Positive Rate
# Specificity or True Negative Rate
# P(A|B) = (P(A) * P(B|A) / P(B)
# P(B) = [P(A) * Sensitivity] + [P(~A) * (1-Specificity))]

# what is the probability of a person get a positive diabetes test result P(Pos)?
# P(D)
p_diabetes = 0.01

# P(~D)
p_no_diabetes = 0.99

# Sensitivity or P(Pos|D)
p_pos_diabetes = 0.9

# Specificity or P(Neg/~D)
p_neg_no_diabetes = 0.9

p_pos = (p_diabetes * p_pos_diabetes) + (p_no_diabetes * (1-p_neg_no_diabetes))
print('The probability of getting a positive test result P(Pos) is: {}',format(p_pos))

#The probability of an individual having diabetes, given that, that individual got a positive test result P(D | Pos)
p_diabetes_pos = (p_diabetes * p_pos_diabetes) / p_pos
print('Probability of an individual having diabetes, given that that individual got a positive test result is:\
',format(p_diabetes_pos))

#Compute the probability of an individual not having diabetes, given that, that individual got a positive test result P(~D | Pos)
# The formula is: P(~D|Pos) = (P(~D) * P(Pos|~D) / P(Pos)
p_no_diabetes_pos = (p_no_diabetes * (1 - p_neg_no_diabetes)) / p_pos
print 'Probability of an individual not having diabetes, given that that individual got a positive test result is:'\
,p_no_diabetes_pos

#calculate the probability Jill Stem say freedom and immigration in his speach using naive bayes P(J|F,I)
p_jill = 0.5
p_jill_freedom = 0.1
p_jill_immigration = 0.1
p_jill_environment = 0.8
p_gary = 0.5
p_gary_freedom = 0.7
p_gary_immigration = 0.2
p_gary_environment = 0.1

# P(J | F, I) = (P(J) * P(F | J) * P(I | J) ) / P(F | I)
# P(F | I) = (P(J) * P(J | F) * P(J | I) ) + (P(G) * P(G | F) * P(G | I))
p_j_text = p_jill * p_jill_freedom * p_jill_immigration
p_g_text = p_gary * p_gary_freedom * p_gary_immigration
p_f_i = p_j_text + p_g_text
print('Probability of words freedom and immigration being said are: ', format(p_f_i))

p_j_fi = (p_jill * p_jill_freedom * p_jill_immigration) / ((p_jill * p_jill_freedom * p_jill_immigration) + (p_gary * p_gary_freedom * p_gary_immigration))
print('The probability of Jill Stein saying the words Freedom and Immigration: ', format(p_j_fi))

p_g_fi = (p_gary * p_gary_freedom * p_gary_immigration) / ((p_jill * p_jill_freedom * p_jill_immigration) + (p_gary * p_gary_freedom * p_gary_immigration))

#implementing naive bayes using scikit learn
naive_bayes = MultinomialNB()
naive_bayes.fit(training_data, y_train)
predictions = naive_bayes.predict(testing_data)

#remember
# Accuracy = true positive predictions / total predictions made
# Precision = [True Positives/(True Positives + False Positives)]
# Recall(Sensitivity) = [True Positives/(True Positives + False Negatives)]
print('Accuracy score: ', format(accuracy_score(y_test, predictions)))
print('Precision score: ', format(precision_score(y_test, predictions)))
print('Recall score: ', format(recall_score(y_test, predictions)))
print('F1 score: ', format(f1_score(y_test, predictions)))
