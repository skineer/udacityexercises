###Renato Pedroso - Capstone Project
This repository contains all the files regarding capstone project for udacity machine learning engineer nanodegree.

#### Dependencies 
All the solution python requirements can be found in the requirements.txt file, in the root of this distribution.

#### INITIAL INSTRUCTIONS FOR CLONING REPO
This repo is git lfs enabled
This means that the reviewer must have the git lfs installed to properly clone this repository to a local machine.

For proper git lfs install, please go to the official [website](https://github.com/git-lfs/git-lfs/releases/tag/v2.3.4) and install accordingly to your operational system

After the installation please do:

git lfs clone 

####Prepare the data
Due to storage limits on udacity website, I need to compress the data file, inside the data folder.

In Windows, 7-zip can be used to uncompress it.
In Linux, a simple tar -xf data/allBTCUSD.tar.gz will suffice

####Folder Structure

* data --> This folder contains the raw data, used in this project, and also the python script used to crawl the data from bitfinex public api
* finalReport --> All the images, and the lyx file, used to construct the final report pdf file
* globalConfig --> Simple python script that has global configurations used in the main script
* proposal --> The files sent on the proposal activity
root
	- finalProject.ipynb --> All the model code
	- finalReport.pdf --> Final capstone report pdf file
	- finalProject.html --> Jupyter html export of finalProject.ipynb
	- requirements.txt --> python libs requirements to run the finalProject.ipynb


