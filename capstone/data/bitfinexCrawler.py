#this script has the objective of crawling the bitfinex api to get all historical data to construct models

import time, requests, datetime,random

#this variables controls what will be retrieved from the API, you can change among all the crypto currencies avl.
cryptoAsset = 'BTC'
realCurrency = 'USD'
saveOutput = '/home/skineer/udacityExercises/capstone/data/all' + str(cryptoAsset) + str(realCurrency)
file = open(saveOutput, 'a')
beginMilisecondTimestamp = 1516696319690 #1358182043000
# structure : PERIOD,MILISECOND_TIMESTAMP,RATE,PRICE
#file.write('PERIOD,TIMESTAMP,RATE,PRICE\n')
file.write('\n')
while True:
    limit = random.randint(990,1000)  # bitfinex max limit = 1000
    try:
        r = requests.get('https://api.bitfinex.com/v2/trades/t'+cryptoAsset+realCurrency+'/hist',
                 params={'start': beginMilisecondTimestamp, 'sort' : 1, 'limit': limit},
                 headers = {"content-type": "application/json"})
    except requests.exceptions.ConnectionError:
        print 'request exception'
        pass
    if r.status_code != 200:
        print r.text
        time.sleep(30)
    else:
        for lista in r.json():
            values = ','.join(str(v) for v in lista)
            file.write(values)
            file.write('\n')
            beginMilisecondTimestamp = lista[1]
        file.flush()
        #if str(datetime.datetime.fromtimestamp(beginMilisecondTimestamp/1000.0))[0:10] == '2018-01-24':
        #    print 'exit condition'
        #    exit(0)
        time.sleep(60)
