# Machine Learning Engineer Nanodegree
## Capstone Proposal
Renato Pedroso  
January 17th, 2018

## Proposal
Predicting Bitcoin's daily max/min price

### Domain Background
Since the world became more global and integrated, new technologies, including financial ones, were implemented to support the new needs. One of them is the creation of cryptocurrencies.

Cryptocurrencies are, nothing more, than digital decentralized assets, that is controlled by cryptography algorithms. These coins have the objective to give an alternative, to users all over the world, in terms of financial transactions in general.

The first cryptocurrency created was Bitcoin. Since its creation, in 2009, a lot of investors have put their money into it, hoping its valorization over time (just like any normal asset bought in any conventional stock market). Try to predict its prices became a question of maximize profits and minimize risks. 

### Problem Statement
Bitcoin's prices tend to vary a lot, this behavior is expected in a market completely opened in terms of speculation. Without a good risk management, and a good enough predicting model running in your favor, it would be really difficult to have good profits in a short term period.

### Datasets and Inputs
Since cryptocurrencies are a new subject, they were "born" in a set of new technologies and methodologies which makes easy the data obtainment by any user across the internet.

There are a lot of trading platforms for the users to chose, I personally trade using bitfinex. The bitfinex data, that will be used in the project, was obtained using their public API service, more information can be found [here](https://docs.bitfinex.com/v2/reference#rest-public-trades). The data was extracted until Jan 17th, 2018 and has 4 fields:

* Period (amount of time the funding transaction was for)
* Timestamp (in milliseconds Unix timestamp)
* Rate (rate at which funding transaction occurred)
* Price

The data is big in size, since it carries information since 2003, but will be trimmed out following simple rule:

* Aggregate the data, by year, month and day, and capture only the max and the min values for that specific day.

Since the extraction script constructed didn't finish as of now, the expectation is that the raw file will have 8M lines and, after the trimming process, it should have something near 1800 lines.

### Solution Statement
Definitely this is a time series type of problem. Normally the proposed solution would be an ARIMA model, which takes into consideration the moving averages of the previous data to make the predictions. For this project I will use [LSTM](https://deeplearning4j.org/lstm.html). This algorithm uses what is called [RNN](https://deeplearning4j.org/lstm.html#recurrent).

LSTM is a type of neural network, that has the objective to imitate the human brain to take decisions / make predictions. It takes into consideration the previous data to take the other ones, which fits perfectly into our proposed problem (predicting max / min prices of active assets).

The reason why RNN type of neural network was chosen is due to past scientific works using this algorithm and having good evaluation metrics in a short term period for stock market prices prediction. This scientific paper can be found [here](http://www.jfas.info/index.php/jfas/article/view/2842/1460) and the reference \#14 of this paper shows a good application on price prediction using RNNs.

The model will have only one input (the current max or min price) and as output it will have the predicted price for the future days to come. Since the output variable wanted is continuous this problem fits the regression type of approach. With that statement is possible to imagine the layout of the neural network (1 input layer, 1 hidden layer and 1 output layer). One neuron for the input layer, the same for the output layer, and, possibly, 4 neurons in the hidden layer.

In any time series type of problem, the order of the data is extremely important. Using this fact, it is impossible to use random train/test splitters since we would lose the order. To avoid it, normal data trimming will be used. The first 80% of the data, that should be the first 1440 days, will be set as training data. The rest, 20% which represents 360 days, will be set as testing data. With this approach the look-ahead bias won't act.

About the tunning of the model, I will grid search the look back parameter, to check if more backwards data improve, or not, the model. Also will add more neurons in the hidden layer.

### Benchmark Model
To benchmark the proposed solution I will use the [sklearn dummy classifier](http://scikit-learn.org/stable/modules/generated/sklearn.dummy.DummyClassifier.html) with 2 parameters:

* “stratified”. With this value set the model will generate predictions by respecting the training set’s class distribution (whatever it is)
* “uniform”: With this value set the model will generate predictions uniformly at random.

Also, to provide a better comparison between the possibilities, I will fit an ARIMA model to compare latter the metrics.

### Evaluation Metrics
To evaluate this solution we will need to use scores for regression problems. For this project I will use [r^2](https://en.wikipedia.org/wiki/Coefficient_of_determination) and [RMSE](http://www.statisticshowto.com/rmse/). Like this we will cover well enough the evaluation metrics part of the project.

### Project Design
Every data science related project must follow a pipeline. The one that I use the most was idealized by EMC. The link of the article can be found [here](https://infocus.emc.com/david_dietrich/the-genesis-of-emcs-data-analytics-lifecycle/). For this project I will use the same strategy, and it is defined like this:

* Discovery

In the discovery phase you learn the business domain, basically. Also, you take your time to learn with the data. So, in this first phase I will take a better look at the data acquired. Will plot a couple preliminary graphs and simple stats to check possible trends or errors.

* Data Preparation

This phase marks the data "massage" that the data scientist must do when using public or proprietary information. In this phase I will basically clean the data, to get only the max and the min values for each day. Also, will prepare my analytics sandbox, which for this project is only a csv file, to be read by [Pandas](https://pandas.pydata.org/). Also, any feature scaling / normalization will be done in this section - LSTMs are sensitive to the scale of inputed data.
In this section, the split technique will be done and, after this phase been finished, the data will be divided into train and test.

* Model Planning

The name is pretty straight forward. This planning has been already done to write this proposal and the model definitions can be found in " Solution Statement", "Benchmark Model" and "Evaluation Metrics" sections.

* Model Building

This is where the actual code starts to arise. This phase marks the coding part of the project (which will be done in Python using various support libraries - such as Pandas, Scikit Learn, Numpy, Keras and etc). In this section I will also check for overfitting / underfitting.

* Communicate the Results

In this phase I will write the conclusion of the project. If the LSTM model failed, or succeeded, in terms of accuracy and comparison with the other ones (for more information check the "Benchmark Model" section).

* Operationalize

This marks the production deploy of the whole solution. For this project I will consider the submission of it.

One important, and interesting, thing about this proposed pipeline is that this phases are completely iterative. This means that I can move forward or backwards on them in case of need.

### References

* https://docs.bitfinex.com/v2/reference#rest-public-trades
* https://deeplearning4j.org/lstm.html
* http://scikit-learn.org
* http://www.statisticshowto.com
* https://infocus.emc.com/david_dietrich/the-genesis-of-emcs-data-analytics-lifecycle/
* https://pandas.pydata.org/
* https://people.duke.edu/~rnau/411arim.htm
* http://www.jfas.info/index.php/jfas/article/view/2842/1460
* http://eprints.covenantuniversity.edu.ng/4112/1/Emerging_Trend.pdf
* https://web.stanford.edu/group/pdplab/pdphandbook/handbookli2.html#XElman90 --> SRN paper
* http://www.wildml.com/2015/09/recurrent-neural-networks-tutorial-part-1-introduction-to-rnns/
* https://www.ijsr.net/archive/v6i4/ART20172755.pdf
* https://www.analyticsvidhya.com/blog/2016/02/time-series-forecasting-codes-python/
* https://brilliant.org/wiki/big-o-notation/
